CREAM
=====

Cream is a Drupal distribution aimed at creating sites for DrupalCamps.

See https://groups.drupal.org/cream-event-management-system for more details
and roadmap.

Installation
------------

The release on drupal.org should contain all of the modules that are part of
Cream. Create your database and install Drupal as normal.

After installation:

- Download and enable your preferred modules and themes for administration, such
  as admin_menu or admin, etc.

Installation from git
---------------------

The git clone contains only the installation profile and the makefile. Turning
this into a full Drupal codebase that you can develop on and make patches or
commits requires a little work:

1. Create a folder for your Drupal development site, eg www/mycream
2. Clone Cream into a subfolder, so you have www/mycream/cream
3. Uncomment the lines in the drupal-org.make file.
   (This appears to be a bug in drush. See https://github.com/drush-ops/drush/issues/563)
4. At the command line, at the root of your future site:

  $ drush make cream/drupal-org.make

5. This will download Drupal core, contrib modules, and libraries. Note that
  unlike the drupal.org packaging process, the cream profile folder itself is
  not moved.
6. Move the cream git clone folder into www/mycream/profiles.
  (You could have put it in that location all along; but this way saves you
  having to create the nested folders.)

7. You can then either install in the UI, or with drush:

$ drush si cream --db-url=mysql://db-user:password@localhost/db-name

You should then download and enable your preferred modules for administration.
We recommend one of the following:

$ drush en -y admin_menu
$ drush en -y admin

Developer notes
---------------

When developing this install profile, you will want to test the complete installation process.

The following removes all files, so that all that is left is this install profile:

chmod sites/default/
chmod sites/default/*
git clean -f -d

This puts you back to a state where you can begin with the initial installation.